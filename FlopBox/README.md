##TP1 SR2
## 31/03/2021

## Mokeddes youva 

## les commandes curl utilisées : 

```
1 -curl -X GET -i http://localhost:8080/myapp/flopResource/connexionFlopBox/anonymous/anonymous => [GET] connexion a la plateforme

```
```

2 - curl -X GET -i 'http://localhost:8080/myapp/flopResource/connexionServer/127.0.0.1/2121/user/12345' =>[GET] connexion au serveur et affichage de son contenu
```
```
3 - curl -X POST -i 'http://localhost:8080/myapp/flopResource/putt/home/mokeddes/Bureau/exemple.txt/127.0.0.1/2121/user/12345' => [POST] mettre un fichier text dans le seveur

```
```
4 -  curl -X POST -i 'http://localhost:8080/myapp/flopResource/putr/home/mokeddes/Bureau/junit/127.0.0.1/2121/user/12345' => [POST]mettre un dossier dans le serveur
    
```
```
5 - curl -X POST -i 'http://localhost:8080/myapp/flopResource/putb/home/mokeddes/Bureau/t.png/127.0.0.1/2121/user/12345' =>[POST] mettre un fichier binaire dans le serveur 
    
```
```
6 - curl -X POST -i 'http://localhost:8080/myapp/flopResource/mkd/monRep/127.0.0.1/2121/user/12345' =>[POST] créer un repertoir
    
```
```
7 - curl -X GET -i 'http://localhost:8080/myapp/flopResource/gett/exemple.txt/127.0.0.1/2121/user/12345' =>[GET] récuperer le fichier 

```
```
8 - curl -X GET -i 'http://localhost:8080/myapp/flopResource/getb/t.png/127.0.0.1/2121/user/12345' =>[GET] récuperer un fichier binaire du serveur
    
```
```
9 -  curl -X GET -i 'http://localhost:8080/myapp/flopResource/list/repAlister/127.0.0.1/2121/user/12345' => lister un repertoir
   
```
```
10 - curl -X GET -i 'http://localhost:8080/myapp/flopResource/delete/junit/127.0.0.1/2121/user/12345' =>[DELETE] supprimer un repertoir
   
```
```
11 - curl -X POST -i 'http://localhost:8080/myapp/flopResource/enr/ftp.ubuntu.com/21/xxx/4555012/toto' => [POST] ajouter un serveur à la plateforme
     
```
```
curl -X GET -i 'http://localhost:8080/myapp/flopResource/list => lister la liste des serveurs
```
``` 
12 - curl -X DELETE -i 'http://localhost:8080/myapp/flopResource/sup/127.0.0.1/toto' => [DELETE] supprimer un serveur
    
```
```
13 - curl -X HEAD -i 'http://localhost:8080/myapp/flopResource/deconnexion/127.0.0.1/2121/user/12345' => [HEAD] se déconnecter proprement du serveur 
     

```

 
### Execution :

## étape 1
```
placez vous dans src ensuite executer les commandes suivantes dans cette ordre :
1- mvn clean compile
2-mvn exec:java
```
## étape2
```
ensuite lancez le serveur python en suivant ces étapes :
~/Bureau$ python3

from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer
authorizer = DummyAuthorizer()
authorizer.add_user("user", "12345", "/home/mokeddes/Bureau/MonServer", perm="elradfmwMT")
handler = FTPHandler
handler.authorizer = authorizer
server = FTPServer(("127.0.0.1", 2121), handler)
server.serve_forever()

une fois c'est fait vous pouvez lancer les commandes dans mon cas j'ai utilisé RestClient....

```

### Quelques fonctionalités implemantés:
- Pour pouvoir ajouter un serveur à la plateforme j'ai ajouté un nouveau paramètre code secret et ce code et demandé pour la suppression le but c'est de sécuriser la supression des serveurs.

```
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("enr/{host}/{port}/{user}/{pass}/{codeSecret}")
	public String StockServer(@PathParam("host") String host,@PathParam("port") int port,@PathParam("user")
	String user,@PathParam("pass") String pass,@PathParam("codeSecret") String codeSecret) throws IOException {
		if(ResourceFlop.isConnectedToFlopBox) {
			ResourceFlop.listServer.add(new Save(host,codeSecret,port,user,pass));
		
		return "serveur ajouté";	
		}
		else {
			throw new IOException("vous étes pas connecté a la platrfome");}
	}


```
Pour povoire modifier un serveur ou bien modifier le contenue de la plateform il est nécessaire de se connecter d'abord à la platerforme
Par exemple dans cette méthode on est obligé de nous connecter à la plateforme pour pourvoir stocker un serveur => if(ResourceFlop.isConnectedToFlopBox) 

```
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("enr/{host}/{port}/{user}/{pass}/{codeSecret}")
	public String StockServer(@PathParam("host") String host,@PathParam("port") int port,@PathParam("user")
	String user,@PathParam("pass") String pass,@PathParam("codeSecret") String codeSecret) throws IOException {
		if(ResourceFlop.isConnectedToFlopBox) {
			ResourceFlop.listServer.add(new Save(host,codeSecret,port,user,pass));
		
		return "serveur ajouté";	
		}
		else {
			throw new IOException("vous étes pas connecté a la platrfome");}
	}


```
### l'architecture du projet se trouve dans le dossier architectureDuProjet ou bien si vous étes sur éclipse vous pouvez l'ouvrire directement src/main/java/UML.ucls

