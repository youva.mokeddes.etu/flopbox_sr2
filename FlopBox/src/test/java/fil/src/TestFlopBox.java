package fil.src;
import static org.junit.Assert.*;

import java.io.IOException;

import org.apache.commons.net.ftp.FTPClient;
import org.junit.Before;
import org.junit.Test;
import fil.src.resource.ResourceFlop;
import fil.src.service.ServiceFlop;

public class TestFlopBox {
	
      @Test(expected = IOException.class)     
      public void testconnexionWithWrogUser() throws IOException {
    	  ServiceFlop service = new ServiceFlop();
      service.connexionToflopbox("wrong", "anonnymous");
    }

      @Test(expected = IOException.class)     
      public void testconnexionWithWrogpassr() throws IOException {
      ServiceFlop service = new ServiceFlop();
      service.connexionToflopbox("anonymous", "wrong");
    }	
	 
      @Test()     
      public void testconnexionWithGoodData() throws IOException {
    	  ServiceFlop service = new ServiceFlop();
      service.connexionToflopbox("anonymous", "anonymous");
    }	

      @Test(expected = IOException.class)     
      public void testconnectToServerFtpWithWrongData() throws IOException {
    	  ServiceFlop service = new ServiceFlop();
      service.connectToServerFTP("x", "x", "x", 0);
    }	
   
      @Test()     
      public void testconnectToServerFtpWithGoodData() throws IOException {
    	  ServiceFlop service = new ServiceFlop();
      service.connectToServerFTP("anonymous", "anonymous", "ftp.ubuntu.com", 21);
    } 
      
      
      
}
