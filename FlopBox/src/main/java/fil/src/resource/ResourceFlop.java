package fil.src.resource;

import java.util.List;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import fil.src.method.Save;

import fil.src.service.ServiceFlop;
/**
 * 
 * @author mokeddes
 *
 */
@Path("flopResource")
public class ResourceFlop {
	/**
	 *class flopboxressource 
	 */
	private Save save;
	public static List<Save> listServer = new ArrayList<>();
	public static boolean isConnectedToFlopBox = false;
	private ServiceFlop plateforme;
	public ResourceFlop(){
    this.plateforme = new ServiceFlop();
	}
    /**
     * cette méthode nous permet de nous connecter à la plateforme
     * @param user
     * @param pass
     * @return
     * @throws IOException 
     */
	@GET
	@Produces(MediaType.TEXT_PLAIN )
	@Path("/connexionFlopBox/{user}/{pass}")
	public String connexionFlopBox(@PathParam("user") String user,@PathParam("pass") String pass) throws IOException {
		if(plateforme.connexionToflopbox(user,pass)) {
			ResourceFlop.isConnectedToFlopBox = true;
			return "vous étes connecté";
		}
		else {
			return "echec de connexion";
		}
	}
	
	/**
	 * cette méthode nous permet d'ajouter un serveur à notre plateforme
	 * @param host
	 * @param port
	 * @param user
	 * @param pass
	 * @param codeSecret
	 * @return
	 * @throws IOException 
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("enr/{host}/{port}/{user}/{pass}/{codeSecret}")
	public String StockServer(@PathParam("host") String host,@PathParam("port") int port,@PathParam("user")
	String user,@PathParam("pass") String pass,@PathParam("codeSecret") String codeSecret) throws IOException {
		if(ResourceFlop.isConnectedToFlopBox) {
			ResourceFlop.listServer.add(new Save(host,codeSecret,port,user,pass));
		
		return "serveur ajouté";	
		}
		else {
			throw new IOException("vous étes pas connecté a la platrfome");}
	}
	
	/**
	 * cette méthode nous permet de lister tous les serveur enregistrés dans notre plateforme
	 * @return
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/list")
	public String ListServer() {
		if(ResourceFlop.isConnectedToFlopBox) {
			String res = "";
			for(Save s: this.listServer) {
				res += s.getHost()+" "+s.getPort()+"\n";
			}
			return res;
		}
		return "echec de connexion";	
	}
	/**
	 * cette méthode nous permet de supprimer un serveur de la plateforme
	 * @param host
	 * @param codeSecret
	 * @return
	 * @throws IOException 
	 */
	@DELETE 
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/sup/{host}/{codeSecret}")
	public String deletServer(@PathParam("host") String host,@PathParam("codeSecret") String codeSecret) throws IOException {
		boolean bool = true;
		int compteur = 0;
		if(ResourceFlop.isConnectedToFlopBox) {
			//on vérifie la connection à la plateforme
			
			while (bool  && compteur < ResourceFlop.listServer.size() ) {
				if(ResourceFlop.listServer.get(compteur).getHost().equals(host)) {
					bool = false;
				}
				compteur++;
			}
			compteur--;
			if(!bool) {
				if(ResourceFlop.listServer.get(compteur).getHost().equals(host) && ResourceFlop.listServer.get(compteur).getcodeSecret().equals(codeSecret) )
				{
					ResourceFlop.listServer.remove(compteur);
					return "le serveur a ete supprimé";
				}
			}
			throw new IOException("le serveur n'existe pas");
			
			
			
		}
		return "echec de connexion";
		
	}
	/**
	 * cette méthode nous permet de nous connecter à un serveur et affiche son contenue.
	 * @param host
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 */
	@GET
	@Path("/connexionServer/{host}/{port}/{user}/{pass}")
	@Produces("text/html")
	public String connexionServeur(@PathParam("host") String host,@PathParam("port") int port,
			@PathParam("user") String user,@PathParam("pass") String pass) throws IOException {
		
		if(ResourceFlop.isConnectedToFlopBox) {
			System.out.println("je suis dans la condition !");
					
			boolean res = this.plateforme.connectToServerFTP(user, pass,host, port);
			
		 	if(res) {
		 		String data = this.plateforme.print_data("/",host,port,user,pass);
				return data; 		
		 		
		 	}
		 	else {
		 		return "échec de connexion au serveur";}
		 	}
		else {
			return "vous n'etes pas connecté à la platefrom";
		}
		

	}
	/**
	 * cette methode nous permet de nous connecter à un serveur enregistré dans la plateform
	 * @return
	 * @throws IOException 
	 * @throws SocketException 
	 */
	@GET
	@Path("/serveurplateform")
	@Produces("text/html")
	public String connectToserverINmyStock() throws SocketException, IOException {
		if(ResourceFlop.isConnectedToFlopBox) {
			Save server = ResourceFlop.listServer.get(0);
			boolean res = this.plateforme.connectToServerFTP(server.getuser(),server.getpass(),server.getHost(),server.getPort());
			if(res) {
				return "vous etes connecté au serveur";
			}
			else {
				return "erreur de connexion au serveur";
			}
			
		}
		return "vous étes pas connecté à la plateform";
		
	}
	
	
	/**
	 * cette method nous permet de lister un repertoir spécifique dans un serveur distant
	 * @param document
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 */
	@GET
	@Produces("text/html")
	@Path("/ls-al/{folder:.*}/{host}/{port}/{user}/{pass}")
	public String cmdLsAl(@PathParam("folder") String document,@PathParam("host") String server,@PathParam("port") int port,@PathParam("user") String user,@PathParam("pass") String pass) throws IOException {
		if(!ResourceFlop.isConnectedToFlopBox) {
			return "vous n'etes pas connecté";
		}
		boolean reponse = this.plateforme.connectToServerFTP(user, pass, server, port);
		if (reponse) {
			
			try {
				this.plateforme.cwdFtpClient(document,server,port,user,pass);
			} catch (IOException e) {
				return this.plateforme.print_data(document,server,port,user,pass);
			}
			
			String data = this.plateforme.print_data(document,server,port,user,pass);
			this.plateforme.getMyFtpClient().disconnect();
			return data;
		
			
		}
		return "connexion au serveur refusé vérifier votre login et mot de pass";
	}
	
	/**
	 * enregistrer un fichier ASCCI dans un serveur distant
	 * @param fichier
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 */
	
	@POST
	@Produces("text/html")
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/putt{fichier:.*}/{host}/{port}/{user}/{pass}")
	public String put_t(@PathParam("fichier") String fichier,@PathParam("host") String server,@PathParam("port") int port,@PathParam("user") String user,@PathParam("pass") String pass) throws IOException{
		if(!ResourceFlop.isConnectedToFlopBox) {
			return "connectez vous à la plateforme svp !";
		}
		boolean reponse = this.plateforme.connectToServerFTP(user, pass, server, port);
		if (reponse) {
			String rep = this.plateforme.stock_file(fichier,false);
			this.plateforme.getMyFtpClient().disconnect();
			return "Fichier sauvegardé avec succée";
		}
		return "connexion au serveur refusé vérifier votre login et mot de pass";
	}
	
	@POST
	@Produces("text/html")
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/putr/{fichier:.*}/{host}/{port}/{user}/{pass}")
	public String stockRep(@PathParam("fichier") String fichier,@PathParam("host") String server,@PathParam("port") int port,@PathParam("user") String user,@PathParam("pass") String pass) throws IOException{
		if(!ResourceFlop.isConnectedToFlopBox) {
			return "Veuillez vous connecter à la platforme";
		}
		boolean bool = this.plateforme.connectToServerFTP(user, pass, server, port);
		if (bool) {
			this.plateforme.stock_folder(fichier);
			//String rep = this.plateforme.stock_folder(fichier);
			 this.plateforme.getMyFtpClient().disconnect();
			 return "sauvgarde du document réussie";
			 //return rep;
		}
		return "connexion au serveur refusé vérifier votre login et mot de pass";
	}
	
	/**
	 * stocket un fichier binaire sur un serveur distant
	 * @param fichier
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 */
     
	@POST
	@Produces("text/html")
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/putb{fichier:.*}/{host}/{port}/{user}/{pass}")
	public String put_tb(@PathParam("fichier") String fichier,@PathParam("host") String server,@PathParam("port") int port,@PathParam("user") String user,@PathParam("pass") String pass) throws IOException{
		if(!ResourceFlop.isConnectedToFlopBox) {
			return "connectez vous à la plateforme svp !";
		}
		boolean reponse = this.plateforme.connectToServerFTP(user, pass, server, port);
		if (reponse) {
			String rep = this.plateforme.stock_file(fichier,true);
			this.plateforme.getMyFtpClient().disconnect();
			return "Fichier sauvegardé avec succée";
		}
		return "connexion au serveur refusé vérifier votre login et mot de pass";
	}
	
	
	
	/**
	 * création d'un repertoir dans un serveur distant
	 * @param fichier
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException 
	 * @throws SocketException 
	 */
	@POST
	@Produces("text/html")
	@Path("/mkd/{fichier:.*}/{host}/{port}/{user}/{pass}")
	public String createRep(@PathParam("fichier") String fichier,@PathParam("host") String server,@PathParam("port") int port,@PathParam("user") String user,@PathParam("pass") String pass) throws SocketException, IOException{
		if(!ResourceFlop.isConnectedToFlopBox) {
			return "Veuillez vous connecter a la platforme";
		}
		boolean bool = this.plateforme.connectToServerFTP(user, pass, server, port);
		if (bool) {
			try {
				if (this.plateforme.makeDirectory_ftpClient(fichier)) {
					this.plateforme.getMyFtpClient().disconnect();
					return "le repertoire a été créé avec succée !";
				}
			} catch (IOException e) {
				e.printStackTrace();
				
			}
		}
		return "échec de création du répertoire";
	}
	
	/**
	 * récuperer un fichier text stocké dans un serveur distant
	 * @param fichier
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException 
	 * @throws SocketException 
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/gett/{fichier:.*}/{host}/{port}/{user}/{pass}")
	public InputStream RecupFilAscci(@PathParam("fichier") String fichier,@PathParam("host") String server,@PathParam("port") int port,@PathParam("user") String user,@PathParam("pass") String pass) throws SocketException, IOException{
		if(ResourceFlop.isConnectedToFlopBox) {
			boolean reponse = this.plateforme.connectToServerFTP(user, pass, server, port);
			if (reponse) {
				try {
					
					return this.plateforme.recover_file(fichier,false);
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}
		}
		return null;
	}

	/**
	 * récuperer un fichier binaire stocké dans un serveur distant
	 * @param fichier
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException 
	 * @throws SocketException 
	 */
	@GET
	@Produces("image/jpeg")
	@Path("/getb/{fichier:.*}/{host}/{port}/{user}/{pass}")
	public InputStream recupFileBinary(@PathParam("fichier") String fichier,@PathParam("host") String server,@PathParam("port") int port,@PathParam("user") String user,@PathParam("pass") String pass) throws SocketException, IOException{
		if(ResourceFlop.isConnectedToFlopBox) {
			boolean bool = this.plateforme.connectToServerFTP(user, pass, server, port);
			if (bool) {
				try {
					return this.plateforme.recover_file(fichier,true);
	
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}
		}
		return null;
	}	
	
	/**
	 * lister un repertoir d'un serveur distant
	 * @param document
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	@GET
	@Path("/list/{folder:.*}/{host}/{port}/{user}/{pass}")
	@Produces("text/html")
	public String listSpecificFolder(@PathParam("folder") String document,@PathParam("host") String server,@PathParam("port") int port,@PathParam("user") String user,@PathParam("pass") String pass) throws UnknownHostException, IOException {
		String data;
		if(!ResourceFlop.isConnectedToFlopBox) {
			return "Veuillez vous connecter";
		}
		
		boolean bool = this.plateforme.connectToServerFTP(user, pass, server, port);
		if (bool) {
			data = this.plateforme.print_data(document,server,port,user,pass);
			this.plateforme.getMyFtpClient().disconnect();
			return data;	 
		}
		
		return "connexion au serveur refusé vérifier votre login et votre mot de pass";
		
	}
	
	/**
	 * supprimer un répertoire d'un serveur distant
	 * @param file
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 */
	@GET
	@Produces("text/html")
	@Path("/delete/{Rep:.*}/{host}/{port}/{user}/{pass}")
	public String deletRep(@PathParam("Rep") String file,@PathParam("host") String server,@PathParam("port") int port,@PathParam("user") String user,@PathParam("pass") String pass) throws IOException {
		
		if(!ResourceFlop.isConnectedToFlopBox) {
			return "Veuillez vous connecter";
		}
		boolean bool = this.plateforme.connectToServerFTP(user, pass, server, port);
		if (bool) {
			 try {
				return this.plateforme.removeDirectory(file);
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			
		}
		return "connexion au serveur refusé vérifier votre login et votre mot de pass";
		
	}
	
	
	/**
	 * affecter un nouveau port a un serveur
	 * @param newPort
	 * @param server
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 */
	@HEAD
	@Produces(MediaType.APPLICATION_XHTML_XML)
	@Path("/chPort/{newPort}/{host}/{port}/{user}/{pass}")
	public String chPort(@PathParam("newPort") int newPort,@PathParam("host") String server,@PathParam("port") int port,@PathParam("user") String user,@PathParam("pass") String pass) throws IOException {
		
		if(!ResourceFlop.isConnectedToFlopBox) {
			return "connectez vous d'abord au serveur";
		}
		boolean bool = this.plateforme.connectToServerFTP(user, pass, server, port);
		if (bool) {
			this.plateforme.port(newPort);
			return "le port a été changé avec succées";
		}
		return "connexion au serveur refusé vérifier votre login et votre mot de pass";
		
	}
	/**
	 * deconnexion d'un serveur 
	 * @param host
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws IOException
	 */
	@HEAD
	@Produces("text/html")
	@Path("/deconnexion/{host}/{port}/{user}/{pass}")
	public String deconnexion(@PathParam("host") String host,@PathParam("port") int port,@PathParam("user") String user,@PathParam("pass") String pass) throws IOException {
		
		if(!ResourceFlop.isConnectedToFlopBox) {
			return "connectez vous d'abord au serveur";
		}
		boolean bool = this.plateforme.connectToServerFTP(user, pass, host, port);
		if (bool) {
			this.plateforme.deconnexion();
			return "deconnexion du serveur";
			
		}
		return "connexion au serveur refusé vérifier votre login et votre mot de pass";
		
	}
	
	
	
}
