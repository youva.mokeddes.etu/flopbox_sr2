package fil.src.service;

import java.awt.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;
import java.io.InputStreamReader;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
/**
 * 
 * @author mokeddes
 *
 */
public class ServiceFlop {
	/**
	 * flopboxserviecer
	 */
	public String nameServer;
	private FTPClient myFtpClient;
	private Socket mySocket;

/**
 * connexion a la plateform
 * @param username
 * @param passW
 * @return
 * @throws IOException 
 */
public boolean connexionToflopbox(String username,String passW) throws IOException {
		
		if(username.equals("anonymous") && passW.equals("anonymous")) {
			return true;
		}
		else {
			throw new IOException("echec de connexion");
		}
	}

/**
 * connexion à un serveur ftp
 * @param user
 * @param pass
 * @param server
 * @param port
 * @return
 * @throws IOException 
 * @throws SocketException 
 */
public boolean connectToServerFTP(String user, String pass,String server, int port) throws SocketException, IOException {
	  this.myFtpClient = new FTPClient();
	  this.nameServer = server;
	  
			System.out.println("juste avant le connect ");
			myFtpClient.connect(server,port);
			System.out.println("juste avant login ");
			myFtpClient.login(user,pass);
			int reponse = myFtpClient.getReplyCode();

		    if(FTPReply.isPositiveCompletion(reponse)) {
		    		        return true;
		    	
		    }
		    else {
		    	
		    throw new IOException("echec de connexion");
		    
		    }
	
}

/**
 * executer la commande port
 * @param port
 * @throws IOException
 */
public void port(int port) throws IOException{
	this.myFtpClient.port(this.myFtpClient.getRemoteAddress(),port);
	
}

/**
 * retourner le contenue du serveur sous forme d'un string
 * @return
 */
public String commande_list_content() {
	
	try {
		this.myFtpClient.enterLocalPassiveMode();
		this.myFtpClient.pasv();
		String res = this.myFtpClient.getReplyString();
		int port = ServiceFlop.getPort(res);
		this.mySocket =new Socket(this.myFtpClient.getRemoteAddress(), port);
		
		this.myFtpClient.list();
		
		BufferedReader MyBufferedReader = new BufferedReader(new InputStreamReader(this.mySocket.getInputStream()));
		
		String result ="";
		
		String myStr = MyBufferedReader.readLine();
		
		while(myStr != null){
			result += myStr+",";
			myStr = MyBufferedReader.readLine();
		}
		myFtpClient.completePendingCommand();
		return result;
		
	} catch (IOException e) {
		//TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return null;
}
/**
 * récuperation du port
 * @param myString
 * @return
 */
public static int getPort(String myString) {
	
	  String [] myStr = myString.substring(myString.indexOf("(")).split(",");
	  return Integer.parseInt(myStr[4])*256+Integer.parseInt(myStr[5].trim().substring(0,myStr[5].trim().length()-2)); 
}


/**
 * cette methode nous renvoie le contenu d'un  repertoir specifique en respectant un format défini
 * @param myFile
 * @param server
 * @param port
 * @param user
 * @param pass
 * @return
 * @throws IOException
 */
public String print_data(String myFile,String server,int port,String user,String pass) throws IOException {

	String final_return="";
	
	this.myFtpClient.changeWorkingDirectory(myFile);
	String path="";
	for(String folder : this.commande_list_content().split(",")){
		  String myStr = folder.split(" ")[0];
		  StringBuilder MyStringBuilder = new StringBuilder();
		  MyStringBuilder.append(folder);
		  MyStringBuilder.reverse();
		  String []tabS = MyStringBuilder.toString().split(" ");
		  MyStringBuilder = new StringBuilder(); 
		  MyStringBuilder.append(tabS[0]);
		  MyStringBuilder.reverse();
		  String document = MyStringBuilder.toString();
		  this.myFtpClient.pwd();
		  path = this.myFtpClient.getReplyString().split(" ")[1];
		  path.replace("\"", "");
		  if (!path.isEmpty()) path = path.substring(1,path.length()-1);
		  final_return += " "+myStr+"==> "+document+"\n";
	}
	if(final_return.equals("")){
		final_return += "the folder is empty";
	}
	
	return final_return;
}

/**
 * vérifie si c'est un repertoir
 * @param data
 * @return
 */

public static boolean isFolder(String data) {
	  String res =data.substring(0,1);
	  return res.equals("d");
}


/**
 * renvoyer le contenu d'un document spécifier en paramétre.
 * @param folder
 * @param serveur
 * @param port
 * @param user
 * @param pass
 * @return
 * @throws IOException
 */
public String cwdFtpClient(String folder,String serveur,int port,String user,String pass) throws IOException{
	this.myFtpClient.cwd(folder);
	return this.print_data(folder,serveur,port,user,pass);
}

/**
 * renvoie le clientFTP
 * @return
 */
public FTPClient getMyFtpClient() {
	return this.myFtpClient;
}


/**
 * cette fonction nous permet d'enregistrer un ficher dans un serveur distant
 * @param fichier
 * @param bool
 * @return
 */
public String stock_file(String fichier,boolean bool){
	   	
	try {
		this.myFtpClient.enterLocalPassiveMode();
		if(bool) {
			//dans le cas ou c'est un fichier binaire ex:image
			this.myFtpClient.setFileType(FTPClient.BINARY_FILE_TYPE);}
		else
			//dans le cas ou ce n'est pas un fichier binaire ex : texrt
			{this.myFtpClient.setFileType(FTPClient.ASCII_FILE_TYPE);
			
			}
		
		this.myFtpClient.pasv();
		if (fichier.charAt(fichier.length()-1) == '/') {
			fichier.substring(fichier.length()-1);
		}
		String[] parse  = fichier.split("/");
		
		int res = parse.length;
		String nom = parse[res-1];
		InputStream myInputstream = new FileInputStream(fichier);
		
		String reponse = this.myFtpClient.getReplyString();
		int port = ServiceFlop.getPort(reponse);
		this.mySocket =new Socket(this.myFtpClient.getRemoteAddress(), port);
		this.myFtpClient.storeFile(nom, myInputstream);
		return this.myFtpClient.getReplyString();

		
	} catch (IOException e) {
		e.printStackTrace();
	}
	
	return null;
	
}
/**
 * cette commande nous permet d'enregistrer un document dans le serveur
 * @param folder
 * @return
 * @throws IOException
 */
public String stock_folder(String folder) throws IOException {
	 
    File fold = new File(folder);
    File[] subFiles = fold.listFiles();
    this.myFtpClient.pwd();
	String chemin = this.myFtpClient.getReplyString().split(" ")[1];
	chemin = chemin.replace("\"", "");
	if(chemin.length() == 1 && chemin.charAt(0) ==('/')) chemin = "";
	else {
		if(chemin.charAt(chemin.length()-1) == '/') {
			chemin = chemin.substring(0,chemin.length()-1);
		}
	}
	String name = fold.getName();
	this.makeDirectory_ftpClient(name);
    this.myFtpClient.cwd(name);
 
	
   if (subFiles != null && subFiles.length > 0) {
        for (File item : subFiles) {
        
        
        	if(!item.getName().equals("..") && !item.getName().equals(".")) {
        		if (item.isFile()) {
	               
	               this.stock_file(folder+"/"+item.getName(), false);
	               return this.myFtpClient.getReplyString();
        		
	                
	              } else {
	            	 //dans le cas ou c'est un autre dossier on refait les memes instructions
	            	  this.stock_folder(folder+"/"+item.getName());
	            	  this.myFtpClient.cwd("..");
	                }}}}
    
    return this.myFtpClient.getReplyString();
   
}
/**
 * exécution de la commande makeDirectory
 * @param file
 * @return
 * @throws IOException
 */
public boolean makeDirectory_ftpClient(String file) throws IOException{
	
    return this.myFtpClient.makeDirectory(file);
}

/**
 * cette fonction nous permet de récuperer le contenue d'un fichier
 * @param filename
 * @param bool
 * @return
 * @throws IOException
 */
public InputStream recover_file(String filename,boolean bool) throws IOException {
	
	try {
		System.out.println("-------------+++++++++++++++++++-*/ ");
		if(bool) {
			//dans le cas ou c'est un fichier ASCII exemple fichier text.
			this.myFtpClient.setFileType(FTPClient.ASCII_FILE_TYPE);
		}
		else {
			//dans le cas ou c'est un fichier binaire ex image.
			this.myFtpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
		}
		
		this.myFtpClient.enterLocalPassiveMode();
		this.myFtpClient.pasv();
		String myStr = this.myFtpClient.getReplyString();
		int port = ServiceFlop.getPort(myStr);
		
		this.mySocket =new Socket(this.myFtpClient.getRemoteAddress(), port);
		return this.myFtpClient.retrieveFileStream(filename);
		
	}
	
	catch (IOException e) {
		e.printStackTrace();
	}
	return null;
}
/**
 * suppression d'un repertoire
 * @param folder
 * @return
 * @throws IOException
 */

public String removeDirectory(String folder) throws IOException{
	
	FTPFile[] data = null;
	try {
		data = this.myFtpClient.listFiles(folder);
	}catch (IOException e) {
		e.printStackTrace();
	}
	String myStr,chemin;
	for(FTPFile ftpFile : data) {
		myStr = ftpFile.getName();
		chemin = folder+"/"+myStr;
		if(ftpFile.isDirectory()) {
			if(!myStr.equals(".")){
				if(!myStr.equals("..")) {
				removeDirectory(chemin);}
			}
		}
		else {
			this.deleteFileFtpClient(chemin);
		}
		
	}
	this.myFtpClient.removeDirectory(folder);
	return this.myFtpClient.getReplyString();
	
}
/**
 * supprimer un fichier dans un serveur distant
 * @param fichier
 * @throws IOException
 */
public void deleteFileFtpClient(String fichier) throws IOException{
	
		 this.myFtpClient.deleteFile(fichier);

	
}

/**
 * executer les commandes rnfr et rnto
 * @param oldFile
 * @param newfile
 * @throws IOException
 */
public void rnto_rnfr(String ancien,String nouveau) throws IOException{
	
	this.myFtpClient.rnfr(ancien);
	this.myFtpClient.rnto(nouveau);
	return;
	
}
/**
 * deconnexion du serveur
 * @throws IOException
 */
public void deconnexion() throws IOException {
	if(this.myFtpClient.isConnected()) {
		System.out.println("je suis dans le if");
		this.myFtpClient.disconnect();
		System.out.println("je suis dans le if 22");
	}
}

}
